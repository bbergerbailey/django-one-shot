from django.urls import path
from todos.views import todo_item_edit, create_todoitem, list_todo, list_detail, create_todolist, update_todolist, delete_todolist

urlpatterns = [
  path("", list_todo, name= "todo_list_list"),
  path("<int:id>/", list_detail,  name="todo_list_detail" ),
  path("create/", create_todolist, name="todo_list_create"),
  path("<int:id>/edit/", update_todolist, name="todo_list_update"),
  path("<int:id>/delete/", delete_todolist, name="todo_list_delete"),
  path("items/create/", create_todoitem, name="todo_item_create" ),
  path("items/<int:id>/edit/", todo_item_edit, name="todo_item_update"),
]
