from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm



# Create your views here.

def list_todo(request):
  todolist =  TodoList.objects.all()
  context = {
    "todolist_object": todolist
  }
  return render(request, "todos/todo_list_list.html", context)

def list_detail(request, id):
  todoitem = get_object_or_404(TodoList, id=id)
  context = {
    "list_detail": todoitem
  }
  return render(request, "todos/todo_list_detail.html", context)

def create_todolist(request):
  if request.method == "POST":
    form = TodoListForm(request.POST)
    if form.is_valid():
      list = form.save()
      return redirect("todo_list_detail", id=list.id)
  else:
    form = TodoListForm()

  context = {
    "form":form
  }
  return render(request, "todos/todo_list_create.html", context)

def update_todolist(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
      form = TodoListForm(request.POST, instance=todolist)
      if form.is_valid():
        form.save()
        return redirect("todo_list_detail", id=id)
    else:
      form = TodoListForm(instance=todolist)
    context = {
      "form":form,
      "todolist_object": todolist,
    }

    return render(request, "todos/todo_list_update.html", context )


def delete_todolist(request, id):
  todolist = TodoList.objects.get(id=id)
  if request.method == "POST":
    todolist.delete()
    return redirect("todo_list_list")
  return render(request, "todos/todo_list_delete.html")

def create_todoitem(request):
  if request.method == "POST":
    form = TodoItemForm(request.POST)
    if form.is_valid():
      item = form.save()
      return redirect("todo_list_detail", id=item.list.id)
  else:
    form = TodoItemForm()
  context = {
      "form":form,
  }
  return render(request, "todos/todo_item_create.html", context)

def todo_item_edit(request, id):
    todoitem = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
      form = TodoItemForm(request.POST, instance=todoitem)
      if form.is_valid():
        form.save()
        return redirect("todo_list_detail", id=id)
    else:
      form = TodoItemForm(instance=todoitem)
    context = {
      "form":form,
      "todoitem_object": todoitem,
    }

    return render(request, "todos/todo_list_update.html", context )
